import localFont from 'next/font/local';

export const fontSans = localFont({
  src: './MabryPro-Regular.woff2',
  variable: '--font-sans',
  display: 'swap',
});
