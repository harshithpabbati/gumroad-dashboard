export type SiteConfig = typeof siteConfig;

export const siteConfig = {
  name: 'Gumroad Dashboard',
  description: 'The new UI for the Gumroad sales dashboard',
};
