'use client';

import {
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';

import { Tooltip as CustomTooltip } from '@/components/stats/graphs/Tooltip';

interface Props {
  data: Record<string, string | number>[];
  dataKey: string;
  prefix?: string;
  suffix?: string;
}

export function LineGraph({ data, dataKey, prefix, suffix }: Props) {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <LineChart
        margin={{
          top: 0,
          bottom: 0,
          left: 0,
          right: 0,
        }}
        data={data}
      >
        <XAxis hide dataKey="name" />
        <YAxis hide />
        <Line
          type="monotone"
          strokeWidth={2}
          dataKey={dataKey}
          stroke="currentColor"
          fill="currentColor"
          activeDot={{
            r: 6,
            style: { fill: 'currentColor', opacity: 0.25 },
          }}
          className="text-primary"
        />
        <Tooltip content={<CustomTooltip prefix={prefix} suffix={suffix} />} />
      </LineChart>
    </ResponsiveContainer>
  );
}
