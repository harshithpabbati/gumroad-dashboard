# Gumroad Sales Dashboard

Found the feature request from here - https://gumroad.nolt.io/866

Build a dashboard for better subscription stats. The “Sales” tab is really bad for this.
I want to be able to easily see:

- How many active subscribers do I have?
- Which Tiers have how many subscribers?
- How many subscribers are on a monthly/yearly plan?
- What is my churn rate?

Basically a proper SaaS/membership dashboard.
